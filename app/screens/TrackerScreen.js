import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import TrackerListCountriesComponent from '../components/TrackerListCountriesComponent';



class TrackerScreen extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
      this.props.navigation.toggleDrawer();
    }

    goToTrackerDetailsScreen(params){
      this.props.navigation.navigate('TrackerDetail',params)
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                  <HeaderComponent showDrawer={this.showDrawer.bind(this)} ></HeaderComponent>
                </View>
                <View style={styles.content}>
                    <TrackerListCountriesComponent goToTrackerDetailsScreen={this.goToTrackerDetailsScreen.bind(this)} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'red',
      justifyContent: 'flex-end',
    },
    header: {
      flex: 1,
    },
    content: {
      flex: 8,
      backgroundColor : '#484747',
    }
  });

export default TrackerScreen
