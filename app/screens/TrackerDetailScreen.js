import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Service from '../../services/Service';

class TrackerDetailScreen extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            countryData: []
        }
    }

    render(){
        const countryName = this.props.route.params
        let countryFetch = new Service("cases",countryName,"")
        countryFetch.getData().then(data => {
            this.state.countryData.push(data)
            this.setState({
                countryData: this.state.countryData,
            })
        }).catch(error => {
            console.log(error)
        })
        let countryData = this.state.countryData[0]
        if(countryData !== undefined){
            const IMAGE_LINK = "https://www.countryflags.io/"+countryData.abbreviation+"/flat/64.png"
            return (
                <View style={styles.content}>
                    <Image source={{uri : IMAGE_LINK}} style={styles.flag}/>
                    <View>
                        <Text style={styles.text}>Country : <Text style={styles.textCountry}> {countryData.country}</Text></Text>
                        <Text style={styles.text}>Continent : <Text style={styles.textCountry}> {countryData.continent}</Text></Text>
                        <Text style={styles.text}>Cases Confirmed : <Text style={styles.textCountry}> {countryData.confirmed}</Text></Text>
                        <Text style={styles.text}>People Recovered : <Text style={styles.textCountry}> {countryData.recovered}</Text></Text>
                        <Text style={styles.text}>Fatalities : <Text style={styles.textCountry}> {countryData.deaths}</Text></Text>
                        <Text style={styles.text}>Updated : <Text style={styles.textCountry}> {countryData.updated}</Text></Text>
                    </View>
                </View>
          )
        }
        return null;
    }
}

const styles = StyleSheet.create({
    flag: {
        width: 64,
        height: 64,
        borderRadius: 16,
        marginRight: 10
    },
    text: {
        fontSize: 20, 
        color: "#072C5F"
    },
    textCountry: {
        fontSize: 20, 
        color: "black"
    },
    header: {
        flex: 1,
    },
    content: {
        flex: 8,
        backgroundColor : '#484747',
        alignItems: "center"
      }
});

export default TrackerDetailScreen
