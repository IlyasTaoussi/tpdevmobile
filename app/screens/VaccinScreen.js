import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import VaccinListCountriesComponent from '../components/VaccinListCountriesComponent';



class VaccinScreen extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
      this.props.navigation.toggleDrawer();
    }

    goToVaccinDetailsScreen(params){
      this.props.navigation.navigate('DetailsVaccin',params)
    }

    render(){
        return (
            <View style={styles.container}>
                <HeaderComponent showDrawer={this.showDrawer.bind(this)} ></HeaderComponent>
                <View style={styles.content}>
                    <VaccinListCountriesComponent goToVaccinDetailsScreen={this.goToVaccinDetailsScreen.bind(this)} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'red',
      justifyContent: 'flex-end',
    },
    header: {
      flex: 1,
    },
    content: {
      flex: 8,
      backgroundColor : '#484747',
    }
  });

export default VaccinScreen
