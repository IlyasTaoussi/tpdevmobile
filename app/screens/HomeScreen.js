import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import HomeComponent from '../components/HomeComponent';

class HomeScreen extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        this.props.navigation.toggleDrawer();
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HeaderComponent showDrawer={this.showDrawer.bind(this)}></HeaderComponent>
                </View>
                <View style={styles.content}>
                   <HomeComponent></HomeComponent>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'red',
      justifyContent: 'flex-end',
    },
    header: {
      flex: 1,
    },
    content: {
      flex: 8,
      backgroundColor : '#484747',
    }
  });

export default HomeScreen
