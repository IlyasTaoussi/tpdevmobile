import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import VaccinScreen from '../screens/VaccinScreen';
import VaccinDetailScreen from '../screens/VaccinDetailScreen';


const Stack = createStackNavigator()
class VaccinNavigator extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <Stack.Navigator initialRouteName="HomeVaccin">
                <Stack.Screen options={{headerShown: false}} name="HomeVaccin" component={VaccinScreen} />
                <Stack.Screen name="DetailsVaccin" component={VaccinDetailScreen} />
            </Stack.Navigator>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default VaccinNavigator
