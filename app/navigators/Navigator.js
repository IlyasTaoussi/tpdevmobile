import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../screens/HomeScreen';
import VaccinNavigator from './VaccinNavigator';
import TrackerNavigator from './TrackerNavigator';

const Drawer = createDrawerNavigator();

class Navigator extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <NavigationContainer theme={MyTheme}>
                <Drawer.Navigator initialRouteName="Home">
                    <Drawer.Screen name="Home" component={HomeScreen} />
                    <Drawer.Screen name="Cases" component={TrackerNavigator} />
                    <Drawer.Screen name="Vaccin" component={VaccinNavigator}/>
                </Drawer.Navigator>
            </NavigationContainer>
        )
    }
}

const MyTheme = {
    dark: false,
    colors: {
      primary: 'rgb(255, 45, 85)',
      background: 'rgb(242, 242, 242)',
      card: '#2f2e2e',
      text: 'white',
      border: '#2f2e2e',
      notification: 'rgb(255, 69, 58)',
    },
  };

export default Navigator
