import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import TrackerScreen from '../screens/TrackerScreen';
import TrackerDetailScreen from '../screens/TrackerDetailScreen';


const Stack = createStackNavigator()
class TrackerNavigator  extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            text: 'Hello from'
        }
    }

    render(){
        return (
            <Stack.Navigator initialRouteName="HomeTracker">
                <Stack.Screen options={{headerShown: false}} name="HomeTracker" component={TrackerScreen} />
                <Stack.Screen name="TrackerDetail" component={TrackerDetailScreen} />
            </Stack.Navigator>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
});

export default TrackerNavigator
