import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

class TrackerComponent extends React.Component {
    constructor(props){
        super(props)
    }

    render(){
        const data = this.props.data
        const IMAGE_LINK = "https://www.countryflags.io/"+data.abbreviation+"/flat/64.png"
        if(data.abbreviation !== undefined){
        return (
            <TouchableOpacity onPress={() => this.props.goToTrackerDetailsScreen(data.country)} style={styles.bord}>
                <Image source={{uri : IMAGE_LINK}} style={styles.flag}/>
                <View>
                    <Text style={styles.text_country}>{data.country}</Text>
                    <Text style={styles.text_confirmed}>Cases Confirmed : {data.confirmed}</Text>
                </View>
            </TouchableOpacity>
        )
        }
        return null;
    }
}

const styles = StyleSheet.create({
    text_country: {
        fontSize: 20,
        height: 30,
        justifyContent: "center",
        flexDirection: "row",
        textAlign: "left",
        paddingLeft: 10,
        color: "white"
    },
    text_confirmed:{
        fontSize: 15,
        height: 20,
        justifyContent: "center",
        flexDirection: "row",
        textAlign: "left",
        paddingLeft: 10,
        marginTop: 20,
        opacity: 0.5, 
        color:"white"
    }
    ,
    flag: {
        width: 64,
        height: 64,
        borderRadius: 16,
        marginRight: 10
    },
    bord: {
        flexDirection: "row",
        padding: 10,
        height: 90,
        borderWidth: 2,
        borderRadius: 6,
        borderColor: "black",
        margin: 10,
        backgroundColor: "#093D86"
    }

});

export default TrackerComponent
