import React from 'react';
import { FlatList, StyleSheet, Text, TextInput, View } from 'react-native';
import Service from '../../services/Service';
import TrackerComponent from './TrackerComponent';
/*Update*/
class TrackerListCountriesComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            allCountries: []
        }

        this.showCountriesList()
    }

    showCountriesList(){
        let dataFetch = new Service("cases","","")
        dataFetch.getData().then(dataArray => {
            this.setState({
                allCountries: dataArray,
                backupCountries: dataArray,
            });
        })
    }

    searchFilterFunction = (text) => {
        const searchedCountries = this.state.backupCountries.filter(item => {
            if(item.country !== undefined){
                const itemCountry = item.country.toUpperCase();
                const textData = text.toUpperCase();
                return itemCountry.indexOf(textData) > -1 ;
            }
            return null;
        });

        this.setState({allCountries: searchedCountries});
    }

    setVaccinComponent = (props) => {
        return (
            <TrackerComponent goToTrackerDetailsScreen={this.props.goToTrackerDetailsScreen} data={props.item}/>
        )
    }

    render(){
        return (
            <View style={styles.container}>
                <TextInput style={styles.input} onChangeText={text => this.searchFilterFunction(text)} placeholder="Enter Country Name" />
                <FlatList style={styles.list}
                    data={this.state.allCountries}
                    renderItem={this.setVaccinComponent.bind(this)}
                    keyExtractor={(item,id) => id.toString()}
                />
            </View>
        )
    }
}



const styles = StyleSheet.create({
    container : {
        backgroundColor: '#484747',
    },  
    text: {
      backgroundColor: "blue",
      color: "white",
      fontSize: 20
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        backgroundColor: "white"
      },
});

export default TrackerListCountriesComponent
