import React from 'react';
import { Image , StyleSheet, Text, View } from 'react-native';

class HomeComponent extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        navigation.openDrawer()
    }

    render(){
        return (
            <View style={styles.containerheader}>
                <Image source={{uri : "https://www.apsf.org/wp-content/uploads/newsletters/2020/3502/coronavirus-covid-19.png"}} style={styles.img}/>
                <Text style={styles.text} >Welcome to our Covid Tracker ! By Ilyas and Birkan</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      color: "white",
      fontSize: 20, 
      textAlign: "center", 
      flex: 1 
    }, 
    img: {
        /* pour le web 
        width: "21%",
        */ 
        /*iphone */ 
        width: "90%",
        height: undefined, 
        flex: 1
    }, 
    containerheader: {
        flex: 1,
        backgroundColor: '#484747',
        alignItems: "center", 
        justifyContent: "center"
    }
});

export default HomeComponent
