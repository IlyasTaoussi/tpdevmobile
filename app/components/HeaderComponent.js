import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

class HeaderComponent extends React.Component {
    constructor(props){
        super(props)
    }

    showDrawer() {
        navigation.openDrawer()
    }

    render(){
        return (
            <View style={styles.containerheader}>
                <Button variant="dark" style={styles.button} color="#484747" title="=" onPress={this.props.showDrawer}></Button>
                <Text style={styles.text}>Covid Tracker</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      color: "white",
      fontSize: 20, 
      textAlign: "center",
      flex: 5
    },
    button: {
        flex: 1, 
        alignItems: "center", 
        color: "white"
    }, 
    containerheader: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#2f2e2e',
        alignItems: "center", 
        justifyContent: "center"
    }
});

export default HeaderComponent
