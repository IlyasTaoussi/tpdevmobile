import fetch from 'node-fetch';
const COVID_TRACK_API = "https://covid-api.mmediagroup.fr/v1/";
//const COVID_API_TEST = "https://covid-api.mmediagroup.fr/v1/vaccines";

class Service{
    constructor(type,country,status){
        this.type = type;
        this.country = country;
        this.status = status;
    }
    
    getData(){
        
        var API_URL = COVID_TRACK_API + "/" + this.type
        if(this.type == "cases"){
            if(this.country != ""){
                API_URL = API_URL + "?country=" + this.country
                //Data Needed for One Country on Input
                return new Promise( (resolve,reject) => {
                    fetch(API_URL).then( httpResponse => {
                        httpResponse.json().then( apiResponse => {
                           let received = apiResponse.All
                           let data ={
                                confirmed: received.confirmed,
                                recovered: received.recovered,
                                deaths: received.deaths,
                                country: received.country,
                                updated: received.updated,
                                abbreviation: received.abbreviation,
                                continent: received.continent
                           }
                            
                            
                            resolve(data)
                        })
                    })
                })
            }
            //Data of All Countries
            else{
            return new Promise( (resolve,reject) => {
                fetch(API_URL).then( httpResponse => {
                    httpResponse.json().then( apiResponse => {
                        let dataArray = []
                        let allCountries = apiResponse
                        let countriesName = Object.keys(allCountries)
                        let i = 0;
                        for(i = 0; i < countriesName.length; i++){
                            let data ={
                                confirmed: allCountries[countriesName[i]].All.confirmed,
                                recovered: allCountries[countriesName[i]].All.recovered,
                                deaths: allCountries[countriesName[i]].All.deaths,
                                country: allCountries[countriesName[i]].All.country,
                                updated: allCountries[countriesName[i]].All.updated,
                                abbreviation: allCountries[countriesName[i]].All.abbreviation,
                                continent: allCountries[countriesName[i]].All.continent
                           }
                           dataArray.push(data)
                        }

                        
                        resolve(dataArray)
                    })
                })
            })
        }
        }/*
        else if(this.type == "history"){
            if(this.status != ""){
                API_URL = API_URL + "?status=" + this.status
                if(this.country != ""){
                    API_URL = API_URL + "&country=" + this.country
                    return new Promise( (resolve,reject) => {
                        fetch(API_URL).then( httpResponse => {
                            httpResponse.json().then( apiResponse => {
                               let received = apiResponse.All
                               let data ={
                                    country: received.country,
                                    dates: received.dates,
                                    abbreviation: received.abbreviation
                               }
                                console.log(data)
                                
                                resolve(data)
                            })
                        })
                    })
                }
                else{
                return new Promise( (resolve,reject) => {
                    fetch(API_URL).then( httpResponse => {
                            httpResponse.json().then( apiResponse => {
                                let dataArray = []
                                let allCountries = apiResponse
                                let countriesName = Object.keys(allCountries)
                                let i = 0;
                                for(i = 0; i < countriesName.length; i++){
                                    let data ={
                                        country: allCountries[countriesName[i]].All.country,
                                        dates: allCountries[countriesName[i]].All.dates,
                                        abbreviation: allCountries[countriesName[i]].All.abbreviation
                                   }
                                   dataArray.push(data)
                                }
                            console.log(dataArray)
                            
                            resolve(dataArray)
                             })
                        })
                    })
                })
            }
            }
            else{
                console.log('Error ! Status Mandatory for History')
            }
        }*/
        else if(this.type == "vaccines"){
            if(this.country != ""){
                API_URL = API_URL + "?country=" + this.country 
                return new Promise( (resolve,reject) => {
                    fetch(API_URL).then( httpResponse => {
                        httpResponse.json().then( apiResponse => {
                            let received = apiResponse.All
                            let data ={
                                administered: received.administered,
                                partiallyVaccinated: received.people_partially_vaccinated,
                                vaccinated: received.people_vaccinated,
                                country: received.country,
                                updated: received.updated,
                                abbreviation: received.abbreviation,
                                continent: received.continent
                           }
                            resolve(data)
                        })
                    })
                })
            }
            else{
            return new Promise( (resolve,reject) => {
                fetch(API_URL).then( httpResponse => {
                    httpResponse.json().then( apiResponse => {
                            let dataArray = []
                            let allCountries = apiResponse
                            let countriesName = Object.keys(allCountries)
                            let i = 0;
                            for(i = 0; i < countriesName.length; i++){
                                let data ={
                                    administered: allCountries[countriesName[i]].All.administered,
                                    partiallyVaccinated: allCountries[countriesName[i]].All.people_partially_vaccinated,
                                    vaccinated: allCountries[countriesName[i]].All.people_vaccinated,
                                    country: allCountries[countriesName[i]].All.country,
                                    updated: allCountries[countriesName[i]].All.updated,
                                    abbreviation: allCountries[countriesName[i]].All.abbreviation,
                                    continent: allCountries[countriesName[i]].All.continent
                               }
                               dataArray.push(data)
                            }
                        
                        resolve(dataArray)
                         })
                    })
                })
            }
        }
        
        else{
            console.log("Error Type !!!")
        }
        

    }
   /*
   getData(){
    return new Promise( (resolve,reject) => {
        fetch(COVID_API_TEST).then( httpResponse => {
            httpResponse.json().then( apiResponse => {
                    let dataArray = []
                    let allCountries = apiResponse
                    let countriesName = Object.keys(allCountries)
                    let i = 0;
                    for(i = 0; i < countriesName.length; i++){
                        let data ={
                            administered: allCountries[countriesName[i]].All.administered,
                            partiallyVaccinated: allCountries[countriesName[i]].All.people_partially_vaccinated,
                            vaccinated: allCountries[countriesName[i]].All.people_vaccinated,
                            country: allCountries[countriesName[i]].All.country,
                            updated: allCountries[countriesName[i]].All.updated,
                            abbreviation: allCountries[countriesName[i]].All.abbreviation,
                       }
                       
                       dataArray.push(data)
                    }
                
                resolve(dataArray)
                 })
            })
        })
   }*/
}

export default Service